<?php
/**
 * パス管理 ユニットテスト
 */
namespace Tests\ScottishFold\Path;

use ScottishFold\Path\Path;

final class PathTest
    extends \PHPUnit_Framework_TestCase
{

    /** test cases */

    public function testNamedDirectoryPathSettable ()
    {
        /* Arrange */
        $directory = '/var/www/site/public/resources/images/';
        /* Act */
        Path::setDirectory('images', $directory);
        /* Assert */
        $this->assertEquals($directory, Path::images());
    }

    public function testDirectorySeparatorAdding ()
    {
        /* Arrange */
        $directory = '/var/www/site/public/resources/images';
        /* Act */
        Path::setDirectory('images', $directory);
        /* Assert */
        $this->assertEquals("{$directory}/", Path::images());
    }

    public function testUnderResourceNameAddable ()
    {
        /* Arrange */
        $directory = '/var/www/site/public/resources/images';
        /* Act */
        Path::setDirectory('images', $directory);
        /* Assert */
        $this->assertEquals("{$directory}/hoge.jpg", Path::images('hoge.jpg'));
    }

    public function testMultipleDirectoriesRegisterable ()
    {
        /* Arrange */
        $www = '/var/www/';
        $log = '/var/log';
        /* Act */
        Path::setDirectory('www', $www);
        Path::setDirectory('log', $log);
        /* Assert */
        $this->assertEquals('/var/www/public', Path::www('public'));
        $this->assertEquals('/var/log/access.log', Path::log('access.log'));
    }

    public function testUndefinedNameReturnNull ()
    {
        /* Assert */
        $this->assertNull(Path::undifinedName('hoge'));
    }

}
