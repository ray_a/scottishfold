<?php
/**
 * パス管理
 */
namespace ScottishFold\Path;

final class Path
{

    /** ディレクトリパス辞書 @var array<string, string> */
    private static $_directories = [];

    /**
     * constructor
     * @return void
     */
    private function __construct ()
    {
    }

    /** behaviors */

    /**
     * ディレクトリパスに名前を付けて設定する
     * @static
     * @param string $name 呼び出しに使用する名称
     * @param string $directoryPath ディレクトリパス
     * @return void
     */
    public static function setDirectory ($name, $directoryPath)
    {
        /* 末尾のディレクトリセパレータを補完して設定 */
        $pattern = '/\\' . DIRECTORY_SEPARATOR . '$/u';
        self::$_directories[$name] = preg_match($pattern, $directoryPath)
            ? $directoryPath : $directoryPath . DIRECTORY_SEPARATOR;
    }

    /**
     * ディレクトリを設定した名前で取得する
     * @param string $name メソッド名
     * @param mixed[] $args OPTIONAL メソッド引数
     * @return string
     */
    public static function __callStatic ($name, array $args = [])
    {
        if (!isset(self::$_directories[$name])) {
            return null;
        }
        return self::$_directories[$name]
            . (empty($args) ? '' : reset($args));
    }

}
